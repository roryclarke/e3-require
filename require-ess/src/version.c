#include "version.h"
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void cleanup_semver(semver_t *s) {
  free(s->version_str);
  free(s);
}

static void init_semver(const char *version, semver_t *s) {
  s->version_str = calloc(strlen(version) + 1, sizeof(char));
  strcpy(s->version_str, version);
  s->test_str = "";

  s->major = 0;
  s->minor = 0;
  s->patch = 0;
  s->build = -1;
}

static void fetch_part(char *version_str, const regmatch_t *groups,
                       unsigned int ix, int *part) {
  char tmp;

  tmp = version_str[groups[ix].rm_eo];
  version_str[groups[ix].rm_eo] = 0;
  *part = atoi(version_str + groups[ix].rm_so);
  version_str[groups[ix].rm_eo] = tmp;
}

int parse_semver(const char *version, semver_t *s) {
  static const char *version_regex =
      "^(([0-9]+)\\.([0-9]+)\\.([0-9]+)(\\+([0-9]+))?)?(.*)$";
  static const unsigned int max_regex_groups = 7 + 1;
  static const unsigned int major_ix = 2;
  static const unsigned int minor_ix = 3;
  static const unsigned int patch_ix = 4;
  static const unsigned int build_ix = 6;
  static const unsigned int test_ix = 7;

  regex_t compiled;
  regmatch_t groups[max_regex_groups];

  init_semver(version, s);

  if (s->version_str == NULL || s->version_str[0] == 0) {
    return 1;
  }

  if (regcomp(&compiled, version_regex, REG_EXTENDED)) {
    return 1;
  }

  if (regexec(&compiled, s->version_str, max_regex_groups, groups, 0) == 0) {
    fetch_part(s->version_str, groups, major_ix, &s->major);
    fetch_part(s->version_str, groups, minor_ix, &s->minor);
    fetch_part(s->version_str, groups, patch_ix, &s->patch);
    if (groups[build_ix].rm_so != (regoff_t)-1) {
      fetch_part(s->version_str, groups, build_ix, &s->build);
    }
    s->test_str = s->version_str + groups[test_ix].rm_so;
  }
  return 0;
}
