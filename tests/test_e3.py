def test_loc(wrappers):
    wrapper = wrappers.get(E3_MODULE_SRC_PATH="test-loc")
    rc, _, errs = wrapper.run_make("build")
    assert rc == 2
    assert 'Local source mode "-loc" has been deprecated' in errs


def test_sitelibs(wrappers):
    # Overwrite the default makefile
    wrapper = wrappers.get()
    with open(wrapper.path / "Makefile", "w") as f:
        f.write(
            f"""
TOP:=$(CURDIR)

E3_MODULE_NAME:={wrapper.name}

include $(REQUIRE_CONFIG)/RULES_E3
include $(REQUIRE_CONFIG)/RULES_CELL
include $(REQUIRE_CONFIG)/DEFINES_FT
include $(REQUIRE_CONFIG)/RULES_PATCH
include $(REQUIRE_CONFIG)/RULES_TEST

include $(REQUIRE_CONFIG)/RULES_DKMS
include $(REQUIRE_CONFIG)/RULES_VARS

include $(REQUIRE_CONFIG)/RULES_DEV
"""
        )
    rc, _, errs = wrapper.run_make("build")
    assert rc == 2
    assert "RULES_E3 should only be loaded from RULES_SITEMODS" in errs


def test_incorrect_module_name(wrappers):
    module_name = "ADCore"

    wrapper = wrappers.get(name=module_name)
    rc, _, errs = wrapper.run_make("build", module_name=module_name)
    assert rc == 2
    assert f"E3_MODULE_NAME '{module_name}' is not valid" in errs
