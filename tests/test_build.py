import re
from pathlib import Path

from .utils import run_ioc_get_output

MODULE_VERSION = "0.0.0+0"
MODULE_VERSION_NO_BUILD = "0.0.0"

RE_MISSING_FILE = "No rule to make target [`']{filename}'"


def create_patch_file(path, desc):
    path.parent.mkdir(parents=True, exist_ok=True)
    patch_file_contents = """
diff --git database.db database.db
index 1806ff6..8701832 100644
--- database.db
+++ database.db
@@ -1,3 +1,3 @@
 record(ai, "TEST") {{
-
+    field(DESC, "{desc}")
 }}
"""
    with open(path, "w") as f:
        f.write(patch_file_contents.format(desc=desc))


def test_patch(wrapper):
    db_path = wrapper.module_dir / "database.db"
    db_file_contents = """record(ai, "TEST") {

}
"""
    with open(db_path, "w") as f:
        f.write(db_file_contents)

    patch_dir = wrapper.path / "patch" / "Site"
    create_patch_file(patch_dir / MODULE_VERSION / "apply.p0.patch", "OK")
    create_patch_file(
        patch_dir / MODULE_VERSION_NO_BUILD / "dont-apply.p0.patch", "Bad"
    )
    create_patch_file(patch_dir / (MODULE_VERSION + "-dont-apply.p0.patch"), "Bad")

    rc, outs, _ = wrapper.run_make(
        "init",
        module_version=MODULE_VERSION,
    )
    assert rc == 0
    assert "You are in the local source mode" in outs

    rc, _, _ = wrapper.run_make("patch", module_version=MODULE_VERSION)
    assert rc == 0
    with open(db_path, "r") as f:
        db_contents = f.read()
    assert 'field(DESC, "OK")' in db_contents
    assert "Bad" not in db_contents

    rc, _, _ = wrapper.run_make("build", module_version=MODULE_VERSION)
    assert rc == 0
    assert any((wrapper.module_dir).glob("O.*"))

    rc, _, _ = wrapper.run_make("cellinstall", module_version=MODULE_VERSION)
    assert rc == 0
    assert any((wrapper.path / "cellMods").glob("**/*.db"))


def test_missing_dbd_file(wrapper):
    wrapper.add_var_to_makefile("DBDS", "nonexistent.dbd")
    rc, _, errs = wrapper.run_make("build")

    assert rc == 2
    assert re.search(
        RE_MISSING_FILE.format(filename=re.escape("../nonexistent.dbd")),
        errs,
    )


def test_missing_source_file(wrapper):
    wrapper.add_var_to_makefile("SOURCES", "nonexistent.c")
    rc, _, errs = wrapper.run_make("build")

    assert rc == 2
    assert re.search(
        RE_MISSING_FILE.format(filename=re.escape("nonexistent.o")),
        errs,
    )


def test_missing_requirement(wrapper):
    wrapper.add_var_to_makefile("REQUIRED", "foo")

    rc, _, errs = wrapper.run_make("build")
    assert rc == 2
    assert "REQUIRED module 'foo' version '' does not exist" in errs


def test_header_install_location(wrapper):
    subdir = wrapper.module_dir / "db" / "subdir"
    subdir.mkdir(parents=True)

    extensions = ["h", "hpp", "hxx", "hh"]
    for ext in extensions:
        wrapper.add_var_to_makefile("HEADERS", f"db/subdir/header.{ext}")
    wrapper.add_var_to_makefile("KEEP_HEADER_SUBDIRS", "db")

    for ext in extensions:
        (subdir / f"header.{ext}").touch()

    rc, *_ = wrapper.run_make("cellinstall")
    assert rc == 0

    cell_path = wrapper.get_env_var("E3_MODULES_INSTALL_LOCATION")

    for ext in extensions:
        assert (Path(cell_path) / "include" / "subdir" / f"header.{ext}").is_file()
        assert not (Path(cell_path) / "include" / f"header.{ext}").is_file()


def test_updated_dependencies(wrappers):
    wrapper_dep = wrappers.get()
    wrapper_main = wrappers.get()

    cell_path = wrapper_main.path / "cellMods"

    old_version = "0.0.0+0"

    wrapper_main.add_var_to_makefile("REQUIRED", wrapper_dep.name)
    wrapper_main.add_var_to_makefile(
        f"{wrapper_dep.name}_VERSION", old_version, modifier=""
    )

    rc, *_ = wrapper_dep.run_make(
        "cellinstall",
        module_version=old_version,
        cell_path=cell_path,
    )
    assert rc == 0

    rc, *_ = wrapper_main.run_make("cellinstall", module_version=old_version)
    assert rc == 0

    new_version = "1.0.0+0"

    rc, *_ = wrapper_dep.run_make(
        "cellinstall",
        module_version=new_version,
        cell_path=cell_path,
    )
    assert rc == 0

    wrapper_main.add_var_to_makefile(
        f"{wrapper_dep.name}_VERSION", new_version, modifier=""
    )

    rc, *_ = wrapper_main.run_make("cellinstall", module_version=new_version)
    assert rc == 0

    rc, outs, _ = run_ioc_get_output(
        wrapper_main.name, new_version, wrapper_main.path / "cellMods"
    )
    assert rc == 0
    assert f"Loaded {wrapper_dep.name} version {new_version}" in outs
